const burger = document.querySelector('.burger');
const menu = document.querySelector('.menu');
const menuItem = document.querySelectorAll('.menuItem');

const toggleMenu = () => {
  burger.classList.toggle('burgerActive');
  menu.classList.toggle('menuActive');
};

const closeMenu = () => {
  burger.classList.remove('burgerActive');
  menu.classList.remove('menuActive');
};

burger.addEventListener('click', toggleMenu);
menuItem.forEach((item) => item.addEventListener('click', closeMenu));
